

from tkinter import *
import lj
import gradec

def jevkvadratu(x,y,mnozica):
    q = False
    for i in mnozica:
        if x > i[0] and x < i[2] and y > i[1] and y < i[3]:
            q = True
            break
    return q

# seznam vseh mest kamor grejo letala oz. od koder pridejo
#iz lj
sc = []

for i in lj.vse:
    if i[0] not in sc:
        sc+=[i[0]]
for i in lj.vse2:
    if i[0] not in sc:
        sc+=[i[0]]
#iz gradca
for i in gradec.prihodi:
    if i[0] not in sc:
        sc+=[i[0]]
for i in gradec.odhodi:
    if i[0] not in sc:
        sc+=[i[0]]



#sprememba iz seznama v slovar
# za lj        
slprihod = {}
for i in lj.vse:
    if i[0] not in slprihod:
        slprihod[i[0]]=[i[1]]
    else:
        slprihod[i[0]]=slprihod[i[0]]+[i[1]]

slodhod = {}
for i in lj.vse2:
    
    if i[0] not in slodhod:
        slodhod[i[0]]=[i[1]]
    else:
        slodhod[i[0]]=slodhod[i[0]]+[i[1]]
# za gradec
grprihod = {}
for i in gradec.prihodi:
    if i[0] not in grprihod:
        grprihod[i[0]]=[i[1]]
    else:
        grprihod[i[0]]=grprihod[i[0]]+[i[1]]

grodhod = {}
for i in gradec.odhodi:
    if i[0] not in grodhod:
        grodhod[i[0]]=[i[1]]
    else:
        grodhod[i[0]]=grodhod[i[0]]+[i[1]]

#slovar vseh prihodov
slprihodov = {}
for i in sc:
    si=[]
    av=[]
    if i in slprihod:
        si+=slprihod[i]
    if i in grprihod:
        av+=grprihod[i]
    slprihodov[i]=[si,av]
    si=[]
    av=[]

#slovar vseh odhodov        
slodhodov = {}
for i in sc:
    si = []
    av = []
    if i in slodhod:
        si += slodhod[i]
    if i in grodhod:
        av += grodhod[i]
    slodhodov[i]=[si,av]
    si=[]
    av=[]
            
    
    

class Postajalisca():
    seznam = sc
    kordinate = []
    #to vedno naredi ko ga zaženeš
    def __init__(self,master):

        self.platno = Canvas(master,width = 900,height = 900,bg="lightgreen")
        self.platno.grid(row=1,column=1)
        self.platno.bind("<Button-1>", self.ispis)
        self.narisi_seznam()
        self.crte()



    def crte(self):
        #navpicne crte
        self.platno.create_line(190,20,190,800)
        self.platno.create_line(350,20,350,800)
        self.platno.create_line(600,20,600,800)
        self.platno.create_line(850,20,850,800)
        #vodoravne crte
        self.platno.create_line(190,100,850,100)
        self.platno.create_line(190,450,850,450)
        self.platno.create_line(190,800,850,800)
        self.platno.create_line(190,20,850,20)
        
        #vsi napisi v platnu
        self.platno.create_text(210,40,text="Letališče",anchor=NW,fill="navy", font=('comic sans ms',15, 'bold italic'))
        self.platno.create_text(440,40,text="Prihodi",anchor=NW,fill="navy", font=('comic sans ms',15, 'bold italic'))
        self.platno.create_text(690,40,text="Odhodi",anchor=NW,fill="navy", font=('comic sans ms',15, 'bold italic'))
        self.platno.create_text(210,250,text="Ljubljana",anchor=NW,fill="Red", font=('comic sans ms',15))
        self.platno.create_text(210,600,text="Gradec",anchor=NW,fill="Red", font=('comic sans ms',15))
        
    def narisi_seznam(self):
        #seznam vseh mest kamor letala letijo oz. od kod prihajajo
        a = 100
        self.platno.create_text(13,a-60,text="Poleti v mesta",anchor=NW,fill="navy", font=('comic sans ms',15, 'bold italic'))
        for i in self.seznam:
            self.platno.create_rectangle(15,a,25,a+10,fill="Red")
            self.platno.create_text(30,a,text=i,anchor=NW,fill="black", font=('Verdana',8, 'bold italic'))
            self.kordinate += [[[15,a,25,a+10]]]
            a += 25

            
    def ispis(self,event):
        x=event.x
        y=event.y
        for i in self.kordinate:
            if jevkvadratu(x,y,i)==True:
                self.platno.delete("all")
                self.crte()        
                self.narisi_seznam()
                if self.seznam[self.kordinate.index(i)] in slprihodov:
                    if slprihodov[self.seznam[self.kordinate.index(i)]][0]!=[]:
                        #narisi
                        a=0
                        for x in slprihodov[self.seznam[self.kordinate.index(i)]][0]:
                            self.platno.create_text(450,150+a,text=str(x),anchor=NW,fill="black", font=('Verdana',10))
                            a+=25
                    if slprihodov[self.seznam[self.kordinate.index(i)]][1]!=[]:
                        #narisi
                        a = 0
                        for x in slprihodov[self.seznam[self.kordinate.index(i)]][1]:
                        
                            self.platno.create_text(450,500+a,text=str(x),anchor=NW,fill="black", font=('Verdana',10))
                            a += 25
                if self.seznam[self.kordinate.index(i)] in slodhodov:
                    if slodhodov[self.seznam[self.kordinate.index(i)]][0]!=[]:
                        #narisi
                        a = 0
                        for x in slodhodov[self.seznam[self.kordinate.index(i)]][0]:
                            self.platno.create_text(700,150+a,text=str(x),anchor=NW,fill="black", font=('Verdana',10))
                            a+=25
                    if slodhodov[self.seznam[self.kordinate.index(i)]][1]!=[]:
                        #narisi
                        a = 0
                        for x in slodhodov[self.seznam[self.kordinate.index(i)]][1]:
                            self.platno.create_text(700,500+a,text=str(x),anchor=NW,fill="black", font=('Verdana',10))
                            a += 25

                        
                break                        


root = Tk()
aplikacija = Postajalisca(root)
root.mainloop()

